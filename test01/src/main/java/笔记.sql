	/*
	  
	  C语言:面向过程编程，无法解决重用，维护，扩展的问题，而且逻辑过于复杂，代码晦涩难懂，
	  java(C++,PHP):面向对象的编程思想
	   	1.以人类解决问题的方法，思路，习惯和步骤来设计相应的应用程序。
	   	2.解决重用，维护，扩展的问题,以提高程序的重用性，灵活性和可扩展性
	   	
	  1.spring 两大核心:
	    
	    1.IOC (Inversion of Control)：控制反转,依赖注入
	    
	    	A.开发人员不用关心对象的生命周期,对象的生命周期交给容器管理
	    	(1).没用spring的时候:ILoginService loginService = new LoingServiceImpl;--->对象是自己创建，维护
	      
	       (2)使用spring
    		@Autowired
    		private ILoginService loginService;
    		
    		B.所有的组件初始化和调用都由容器负责。组件处在一个容器当中，由容 器负责管理
    			容器控制程序之间的关系，而非传统实现中，由程序代码直接操控
	       
	    2.AOP(Aspect-Oriented Programming):面向切面编程
	    	A.把系 统中的某些特定的重复性行为封装在某个模块
	    	B.解决重用，维护，扩展的问题,以提高程序的重用性，灵活性和可扩展性
	  
	  
	  
	  2.IOC:
	  
	   1.spring的三种注入方式:
	  		  setter注入：1.给需要注入的值set方法  2.在配置bean时给property值  3.对象中的属性名称和property中的name值要一致
	      	     构造方法注入：1.给需要注入的值通过构造方法设值 2.bean文件中constructor-arg 设值
	  		     基于注解的注入：@Resource或者@Autowired
	   2.spring创建bean：1.通过配置文件：<bean id="userService" class="com.robert.spring.day01.UserServiceImpl">
	  					  2.通过注解方式:@Component ：标准一个普通的spring Bean类。 
	 									@Repository：标注一个DAO组件类。 
	 									@Service：标注一个业务逻辑组件类。 
	 									@Controller：标注一个控制器组件类。 
	 									
	 									@Component可以代替@Repository、@Service、@Controller，因为这三个注解是被@Component标注
	 									
	 3.AOP:
	  
	  
	 */