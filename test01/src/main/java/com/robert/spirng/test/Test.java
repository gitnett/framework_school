package com.robert.spirng.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.robert.spring.day01.IUserService;

public class Test {
	
	public static void main(String[] args) {
		
		/*IUserService userService = new UserServiceImpl();
		userService.login();*/
		
		ApplicationContext context = new ClassPathXmlApplicationContext("application.xml");
		IUserService peopleService = (IUserService) context.getBean("peopleService");
		IUserService userService = (IUserService) context.getBean("userService");
		IUserService custService = (IUserService) context.getBean("custService");
		
		peopleService.login();
		userService.login();
		custService.login();
		
	}

}
