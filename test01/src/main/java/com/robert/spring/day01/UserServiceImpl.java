package com.robert.spring.day01;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.robert.dao.IUserDao;

@Service("userService")
public class UserServiceImpl implements IUserService{
	
	@Resource
	IUserDao userDao;
	
/*  set注入
	public void setUserDaoImpl(IUserDao userDaoImpl) {
		this.userDaoImpl = userDaoImpl;
	}
*/

/*  构造方法注入
	public UserServiceImpl(IUserDao userDaoImpl){
		this.userDaoImpl = userDaoImpl;
	}
*/

	public String login() {
		System.out.println("UserServiceImpl.......");
		return userDao.login();
	}

}
