package com.robert.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.robert.aspect.MyLog;
import com.robert.spring.day01.IUserService;

@Component
@RequestMapping("")
public class TestController {
	
	@Resource
	IUserService userService;




	@RequestMapping("/cust")
	@ResponseBody
	@MyLog(value="4444444")
	public String cust() {
		return userService.login();
	}

	@RequestMapping("/user")
	@ResponseBody
	public String user() {
		return "user";

	}

	@RequestMapping("/people")
	@ResponseBody
	public String people() {
		return "people";
	}

}
