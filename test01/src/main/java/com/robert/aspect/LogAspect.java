package com.robert.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogAspect {
	
    //execution(* me.lichunlong.spring.service.*.*(..)) && @annotation(log)
    @Pointcut("@annotation(com.robert.aspect.MyLog)")
    private void advice() {
    }
    
    @Before("advice()")
    public void doBefore(JoinPoint jp) {
        System.out.println("rrrrrrrrrrrrrrrrrrrrr");
    }
   

}
