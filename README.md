package com.robert.cache;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

public class CacheStore implements ICacheStore{
	
	 private final Cache cache;
	  
	  public CacheStore(String name) {
		    CacheManager manager = CacheManager.getInstance();
		    Cache c = manager.getCache(name);
		    if (c == null)
		    {
		      manager.addCache(name);
		      c = manager.getCache(name);
		    }
		    this.cache = c;
		    if (this.cache == null) {
		      throw new IllegalStateException("Can not create ehCache entity! Name[" + name + "]");
		    }
	  }
	  
	  public Object getCacheData(Object key){
	    Element element = this.cache.get(key);
	    if (element == null) {
	      return null;
	    }
	    return element.getObjectValue();
	  }
	  
	  public boolean isDataInCache(Object key){
	    return this.cache.isKeyInCache(key);
	  }
	  
	  public void putCacheData(Object key, Object data){
	    this.cache.put(new Element(key, data));
	  }
	  
	  public void shutdown(){
	    this.cache.dispose();
	  }
	  
	  public void removeCacheData(Object key) {
		  
	    if (this.cache != null) {
	      this.cache.remove(key);
	    }
	    
	  }
	  
	  public void removeAllCacheData(){
	    this.cache.removeAll();
	  }
}


package com.robert.cache;

public class CacheEntry {
	  Object data;
	  Object key;
	  
	  public Object getData()
	  {
	    return this.data;
	  }
	  
	  public Object getKey()
	  {
	    return this.key;
	  }
	  
	  public void setData(Object data)
	  {
	    this.data = data;
	  }
	  
	  public void setKey(Object key)
	  {
	    this.key = key;
	  }
}


package com.robert.cache;

public interface ICacheDataProvider {
	 public abstract Object getData(Object paramObject);
}


package com.robert.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MyCacheManager {
	
	  static MyCacheManager singleton = new MyCacheManager();
	  
	  private Map<String, ICache> storages = new ConcurrentHashMap();
	  
	  public static MyCacheManager getInstance(){
	    return singleton;
	  }
	  
	  
	  public ICache getCache(String name){
	    return (ICache)this.storages.get(name);
	  }
	  
	  public void regCache(String name, ICache icache){
	    this.storages.put(name, icache);
	  }
}



package com.robert.cache;

public interface ICache {
	
	  public abstract String getCacheName();
	  
	  public abstract void setCacheName(String paramString);
	  
	  public abstract ICacheStore getCacheStore();
	  
	  public abstract void setCacheStore(ICacheStore paramICacheStore);
	  
	  public abstract ICacheDataProvider getCacheDataProvider();
	  
	  public abstract void setCacheProvider(ICacheDataProvider paramICacheDataProvider);
	  
	  public abstract boolean containsKey(Object paramObject);
	  
	  public abstract Object getData(Object paramObject);
	  
	  public abstract void setData(Object paramObject1, Object paramObject2);
	  
	  public abstract void removeData(Object paramObject);
	  
	  public abstract void refresh();
	  
	  public abstract void shutdown();
}



package com.robert.cache;

public class BaseDataCacheEntry  extends CacheEntry{
	  long createTime = System.currentTimeMillis();
	  
	  public boolean isExpired(long ttl)
	  {
	    if (ttl < 0L) {
	      return false;
	    }
	    return System.currentTimeMillis() - this.createTime > ttl;
	  }
	  
	  public long getCreateTime()
	  {
	    return this.createTime;
	  }
	  
	  public void setCreateTime(long createTime)
	  {
	    this.createTime = createTime;
	  }

}









package com.robert.cache;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class BaseCache implements ICache{
	
	  //缓存名称
	  String name;
	  
	  //缓存存储
	  ICacheStore cacheStore;
	  
	  //缓存提供者
	  ICacheDataProvider cacheProvider;
	  
	  protected final Log logger = LogFactory.getLog(BaseCache.class);
	  
	  public String getCacheName(){
	    return this.name;
	  }
	  
	  public void setCacheName(String name){
	    this.name = name;
	  }
	  
	  public ICacheStore getCacheStore() {
	    return this.cacheStore;
	  }
	  
	  public void setCacheStore(ICacheStore cacheStore) {
	    this.cacheStore = cacheStore;
	  }
	  
	  public ICacheDataProvider getCacheDataProvider(){
	    return this.cacheProvider;
	  }
	  
	  public void setCacheProvider(ICacheDataProvider dataProvider) {
	    this.cacheProvider = dataProvider;
	  }
	  
	  public Object getData(Object key) {
	    if (this.cacheProvider == null) {
	      return null;
	    }
	    Object data = this.cacheStore.getCacheData(key);
	    if (data == null)
	    {
	      data = this.cacheProvider.getData(key);
	      this.cacheStore.putCacheData(key, data);
	    }
	    return data;
	  }
	  
	  public void setData(Object key, Object data){
	    this.cacheStore.putCacheData(key, data);
	  }
	  
	  public void refresh() {}
	  
	  public void shutdown(){
	    if (this.cacheStore != null) {
	      this.cacheStore.shutdown();
	    }
	  }
	  
	  public void removeData(Object key){
	    if (this.cacheStore != null) {
	      this.cacheStore.removeCacheData(key);
	    }
	  }
	  
	  public boolean containsKey(Object key)
	  {
	    if (this.cacheStore != null)
	    {
	      Object data = this.cacheStore.getCacheData(key);
	      return data != null;
	    }
	    return false;
	  }
}

package com.robert.cache;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public abstract class BaseCacheDataProvider implements ICacheDataProvider, InitializingBean, DisposableBean{
	
	  protected final Log logger = LogFactory.getLog(BaseCacheDataProvider.class);
	 
	  String cacheName;
	  
	  protected ICache cache = null;
	  
	  public String getCacheName(){
	    return this.cacheName;
	  }
	  
	  public void setCacheName(String cacheName){
	    this.cacheName = cacheName;
	  }
	  
	  public void afterPropertiesSet()throws Exception{
		  
			//开始注册  
		    ICache cache = initializeCache();
		    if (cache != null) {
		      MyCacheManager.getInstance().regCache(cache.getCacheName(), cache);
		    }
	  }
	  
	  
	  protected ICache initializeCache(){
	    ICache cache = getCache();
	    if (cache == null)
	    {
	      cache = createCache();
	      cache.setCacheProvider(this);
	      setCache(cache);
	    }
	    return cache;
	  }
	  
	  protected ICache createCache(){
	    ICache cache = new BaseCache();
	    cache.setCacheName(this.cacheName);
	    CacheStore store = new CacheStore(this.cacheName);
	    cache.setCacheStore(store);
	    return cache;
	  }
	  
	  protected ICache getCache() {
	    return this.cache;
	  }
	  
	  protected void setCache(ICache cache)
	  {
	    this.cache = cache;
	  }
	  
	  public void destroy()
	    throws Exception
	  {
	    ICache cache = getCache();
	    if (cache != null) {
	      cache.shutdown();
	    }
	  }
}


package com.robert.cache;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class BaseDataCacheDataProvider  extends BaseCacheDataProvider{
	
	  protected final Log logger = LogFactory.getLog(BaseDataCacheDataProvider.class);
	  
	  //过期时间 分钟
	  long ttl;
	  
	  protected ICache createCache(){
	    BaseDataCache cache = new BaseDataCache();
	    cache.setCacheName(this.cacheName);
	    cache.setTtl(this.ttl * 60L * 1000L);
	    return cache;
	  }
	  
	  public long getTtl(){
	    return this.ttl;
	  }
	  
	  public void setTtl(long ttl){
	    this.ttl = ttl;
	  }
}
package com.robert.cache;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class BaseDataCache  extends BaseCache{
	
	  BaseDataCacheEntry cacheObject = new BaseDataCacheEntry();
	  protected final Log logger = LogFactory.getLog(BaseDataCache.class);
	  long ttl = 60000L;
	  
	  public long getTtl()
	  {
	    return this.ttl;
	  }
	  
	  public void setTtl(long ttl)
	  {
	    this.ttl = ttl;
	  }
	  
	  public Object getData(Object key)
	  {
	    if (this.cacheProvider == null) {
	      return null;
	    }
	    if ((this.cacheObject.getData() == null) || (this.cacheObject.isExpired(this.ttl))) {
	      synchronized (this)
	      {
	        if ((this.cacheObject.getData() == null) || (this.cacheObject.isExpired(this.ttl)))
	        {
	          Object data = this.cacheProvider.getData(key);
	          this.cacheObject.setData(data);
	          this.cacheObject.setKey(key);
	          this.cacheObject.setCreateTime(System.currentTimeMillis());
	        }
	      }
	    }
	    return this.cacheObject.getData();
	  }
	  
	  public void refresh()
	  {
	    synchronized (this)
	    {
	      if (this.cacheObject.getData() != null)
	      {
	        Object data = this.cacheProvider.getData(this.cacheObject.getKey());
	        this.cacheObject.setData(data);
	        this.cacheObject.setCreateTime(System.currentTimeMillis());
	      }
	    }
	  }
	  
	  public void removeData(Object key)
	  {
	    this.cacheObject.setData(null);
	  }
}
